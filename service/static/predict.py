import static.train_model as train


# predict using mnb
def gender_predictor_mnb(a, cv, clf):
    test_name = [a]
    vector = cv.transform(test_name).toarray()
    if clf.predict(vector) == 0:
        return "Female"
    else:
        return "Male"


# predict use decision tree
def gender_predictor_dt(a, dv, dclf):
    test_name = [a]
    transform_dv =dv.transform(train.features(test_name))
    vector = transform_dv.toarray()
    if dclf.predict(vector) == 0:
        return "Female"
    else:
        return "Male"
