# Name-Prediction-App

Name predictor can be used to predict gender by first name

The app can be run locally by a few ways:

#Run app with Docker Compose
    
This way can run both service and ui at one go. In the project directory, run
   
```bash
docker-compose up
```
    
    
Service is ready when you see
   
```bash
service_1  |  * Running on http://0.0.0.0:5000/ (Press CTRL+C to quit)
```

ui is ready when you see

```bash
ui_1  | Compiled successfully!
```

open http://localhost:3000  at your browser to use the app!


#Run app by local build script

This way service and ui can be run separately 

- To run ui:

```bash
cd ui
sh build.sh
```

- To run backend service

```bash
cd service
sh build.sh
```

open http://localhost:3000  at your browser to use the app!

api can be called from http://127.0.0.1:5000/prediction/

api document can be viewed from http://localhost:5000/

#Run APP from Docker Repo

The images for ui and service have been push to my docker repo zhenliu865/gender_predictor which can be access by 

```bash
docker pull zhenliu865/gender_predictor:gender_predictor_service_latest
docker pull zhenliu865/gender_predictor:gender_predictor_ui_latest
```