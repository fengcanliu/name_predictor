import React, {Component} from 'react';
import './App.css';
import Form from 'react-bootstrap/Form';
import Col from 'react-bootstrap/Col';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Button from 'react-bootstrap/Button';
import 'bootstrap/dist/css/bootstrap.css';

class App extends Component {

    constructor(props) {
        super(props);

        this.state = {
            isLoading: false,
            formData: {
                name: '',
                method: 'Decision Tree'
            },
            nameEmptyError: null,
            nameValueError: null,
            result: ""
        };
    }

    handleChange = (event) => {
        const value = event.target.value;
        const name = event.target.name;
        var formData = this.state.formData;
        formData[name] = value;
        this.setState({
            formData
        });
    }

    handlePredictClick = (event) => {
        const formData = this.state.formData;
        this.setState({isLoading: true});
        if (formData.name === null || formData.name === '') {
            this.setState({
                nameEmptyError: 'Name is required',
            });
            this.setState({result: "", isLoading : false});
        } else if (!formData.name.match(/^[a-z ,.'-]+$/i)) {
            this.setState({
                nameValueError: 'Invalid Name'
            })
             this.setState({result: "", isLoading: false});
        } else {
            fetch('http://127.0.0.1:5000/prediction/',
                {
                    headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json'
                    },
                    method: 'POST',
                    body: JSON.stringify(formData)
                })
                .then(response => response.json())
                .then(response => {
                    this.setState({
                        result: response.result,
                        isLoading: false,
                        nameEmptyError: null,
                        nameValueError: null

                    });
                });
        }

    }

    handleCancelClick = (event) => {
        this.setState({result: "", isLoading: false,  nameEmptyError: null,
                        nameValueError: null, formData: {name: ''}});
    }

    render() {
        const isLoading = this.state.isLoading;
        const formData = this.state.formData;
        const result = this.state.result;
        const nameEmptyError = this.state.nameEmptyError
        const nameValueError = this.state.nameValueError
        return (
            <Container>
                <div>
                    <h1 className="title">Name Gender predictor</h1>
                </div>
                <div className="content">
                    <Form>
                        <Form.Row>
                            <Form.Group as={Col}>
                                <Form.Label>Name</Form.Label>
                                <Form.Control
                                    required
                                    type="text"
                                    placeholder="Please enter a first name for us to predict gender"
                                    name="name"
                                    value={this.state.formData.name}
                                    onChange={this.handleChange}/>
                            </Form.Group>
                            <Form.Control.Feedback type="invalid">
                                Please enter a name.
                            </Form.Control.Feedback>
                        </Form.Row>
                        <h5 id="nameError">{nameEmptyError}</h5>
                        <h5 id="nameError">{nameValueError}</h5>
                        <Form.Row>
                            <Form.Group as={Col}>
                                <Form.Label>Method</Form.Label>
                                <Form.Control
                                    as="select"
                                    value={formData.method}
                                    name="method"
                                    defaultValue={this.state.formData.method}
                                    placeholder="Select a Method"
                                    onChange={this.handleChange}>
                                    <option disabled>Select Method</option>
                                    <option value="Decision Tree">Decision Tree</option>
                                    <option value="MNB">MNB</option>
                                </Form.Control>
                            </Form.Group>
                        </Form.Row>
                        <Row>
                            <Col>
                                <Button
                                    block
                                    variant="success"
                                    disabled={isLoading}
                                    onClick={!isLoading ? this.handlePredictClick : null}>
                                    {isLoading ? 'Making prediction' : 'Predict'}
                                </Button>
                            </Col>
                            <Col>
                                <Button
                                    block
                                    variant="danger"
                                    disabled={isLoading}
                                    onClick={this.handleCancelClick}>
                                    Reset prediction
                                </Button>
                            </Col>
                        </Row>
                    </Form>
                    {result === "" ? null :
                        (<Row>
                            <Col className="result-container">
                                <h5 id="result">{result}</h5>
                            </Col>
                        </Row>)
                    }
                </div>
            </Container>
        );
    }
}

export default App;
